package task;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public class ChildTreeNode<T> extends TreeNode<T> {

    public ChildTreeNode(T value) {
        super(value);
    }

    @Override
    public boolean isRoot() {
        return false;
    }
}
