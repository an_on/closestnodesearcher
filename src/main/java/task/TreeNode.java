package task;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import static lombok.AccessLevel.PRIVATE;

@Getter @Setter(PRIVATE)
@EqualsAndHashCode
public abstract class TreeNode<T> {

    private TreeNode<T> left;
    private TreeNode<T> right;
    private TreeNode<T> parent;
    @Setter
    private T value;

    abstract boolean isRoot();

    public TreeNode(T value) {
        this.value = value;
    }

    public void addRight(TreeNode<T> right) {
        if (right == null) {
            throw new IllegalArgumentException("ChildTreeNode should not be null");
        }

        right.setParent(this);
        setRight(right);
    }

    public void addLeft(TreeNode<T> left) {
        if (left == null) {
            throw new IllegalArgumentException("ChildTreeNode should not be null");
        }

        left.setParent(this);
        setLeft(left);
    }

}
