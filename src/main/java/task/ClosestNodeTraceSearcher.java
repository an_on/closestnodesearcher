package task;


import java.util.*;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.joining;

public class ClosestNodeTraceSearcher<T> {

    public String findClosestNodeTrace(TreeNode<T> root) {
        if (root == null || !root.isRoot()) {
            throw new IllegalArgumentException("Root node is missing");
        }

        TreeNode<T> closestNode = findClosestNode(root);

        return closestNode == null ? "" : findNodeTrace(closestNode);
    }

    private TreeNode<T> findClosestNode(TreeNode<T> root) {
        Queue<TreeNode<T>> queue = new LinkedList<>(singletonList(root));

        return findClosestNode(queue);
    }

    private TreeNode<T> findClosestNode(Queue<TreeNode<T>> queue) {
        TreeNode<T> node = queue.poll();

        if (hasNeighbours(node)) {
            if (node.getLeft() != null) {
                queue.add(node.getLeft());
            }

            if (node.getRight() != null) {
                queue.add(node.getRight());
            }

            return findClosestNode(queue);
        }

        return node;
    }

    private String findNodeTrace(TreeNode<T> node) {
        List<T> list = new ArrayList<>();
        TreeNode<T> currentNode = node;

        while (!currentNode.isRoot()) {
            list.add(currentNode.getValue());
            currentNode = currentNode.getParent();

            if (currentNode.isRoot()) {
                list.add(currentNode.getValue());
            }
        }

        Collections.reverse(list);

        return list.stream()
                .map(String::valueOf)
                .collect(joining("-"));
    }

    private boolean hasNeighbours(TreeNode<T> node) {
        return node != null && (node.getLeft() != null || node.getRight() != null);
    }
}
