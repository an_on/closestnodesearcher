package task;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public class RootTreeNode<T> extends TreeNode<T> {

    public RootTreeNode(T value) {
        super(value);
    }

    @Override
    public boolean isRoot() {
        return true;
    }
}
