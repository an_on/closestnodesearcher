package task;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class ClosestNodeTraceSearcherTest {
    private static final ClosestNodeTraceSearcher<String> NODE_SEARCHER = new ClosestNodeTraceSearcher<>();

    @Test
    public void shouldReturnResultFromTask() {
        String result = NODE_SEARCHER.findClosestNodeTrace(createMockNodeFromExample());

        assertThat(result).isEqualTo("A-C-F");
    }

    @Test
    public void shouldReturnEmptyResultForRootOnly() {
        String result = NODE_SEARCHER.findClosestNodeTrace(new RootTreeNode<>("A"));

        assertThat(result).isEmpty();
    }

    @Test
    public void shouldReturnResultForRightOnly() {
        String result = NODE_SEARCHER.findClosestNodeTrace(createMockNodeRight());

        assertThat(result).isEqualTo("A-C-G");
    }

    @Test
    public void shouldReturnResultForFourthLevel() {
        String result = NODE_SEARCHER.findClosestNodeTrace(createMockNodeFourthLvl());

        assertThat(result).isEqualTo("A-B-D-H");
    }

    @Test
    public void shouldReturnResultForNullValues() {
        String result = NODE_SEARCHER.findClosestNodeTrace(createMockNodeNullValues());

        assertThat(result).isEqualTo("null-null");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailOnNullInput() {
        NODE_SEARCHER.findClosestNodeTrace(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailForNullLeft() {
        NODE_SEARCHER.findClosestNodeTrace(createMockNodeNullLeftNode());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailForNullRight() {
        NODE_SEARCHER.findClosestNodeTrace(createMockNodeNullRightNode());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailForNonRootNode() {
        NODE_SEARCHER.findClosestNodeTrace(new ChildTreeNode<>("A"));
    }

    private TreeNode<String> createMockNodeFromExample() {
        TreeNode<String> a = new RootTreeNode<>("A");
        TreeNode<String> b = new ChildTreeNode<>("B");
        TreeNode<String> c = new ChildTreeNode<>("C");
        TreeNode<String> d = new ChildTreeNode<>("D");
        TreeNode<String> e = new ChildTreeNode<>("E");
        TreeNode<String> f = new ChildTreeNode<>("F");
        TreeNode<String> g = new ChildTreeNode<>("G");
        TreeNode<String> h = new ChildTreeNode<>("H");
        TreeNode<String> i = new ChildTreeNode<>("I");
        TreeNode<String> j = new ChildTreeNode<>("J");

        d.addLeft(h);
        d.addRight(i);

        e.addLeft(j);

        b.addLeft(d);
        b.addRight(e);

        c.addLeft(f);
        c.addRight(g);

        a.addLeft(b);
        a.addRight(c);

        return a;
    }

    private TreeNode<String> createMockNodeRight() {
        TreeNode<String> a = new RootTreeNode<>("A");
        TreeNode<String> b = new ChildTreeNode<>("B");
        TreeNode<String> c = new ChildTreeNode<>("C");
        TreeNode<String> d = new ChildTreeNode<>("D");
        TreeNode<String> e = new ChildTreeNode<>("E");
        TreeNode<String> f = new ChildTreeNode<>("F");
        TreeNode<String> g = new ChildTreeNode<>("G");
        TreeNode<String> h = new ChildTreeNode<>("H");
        TreeNode<String> i = new ChildTreeNode<>("I");
        TreeNode<String> j = new ChildTreeNode<>("J");
        TreeNode<String> k = new ChildTreeNode<>("K");

        d.addLeft(h);
        d.addRight(i);

        e.addLeft(j);

        b.addLeft(d);
        b.addRight(e);

        c.addLeft(f);
        c.addRight(g);

        a.addLeft(b);
        a.addRight(c);

        f.addRight(k);

        return a;
    }

    private TreeNode<String> createMockNodeFourthLvl() {
        TreeNode<String> a = new RootTreeNode<>("A");
        TreeNode<String> b = new ChildTreeNode<>("B");
        TreeNode<String> c = new ChildTreeNode<>("C");
        TreeNode<String> d = new ChildTreeNode<>("D");
        TreeNode<String> e = new ChildTreeNode<>("E");
        TreeNode<String> f = new ChildTreeNode<>("F");
        TreeNode<String> g = new ChildTreeNode<>("G");
        TreeNode<String> h = new ChildTreeNode<>("H");
        TreeNode<String> i = new ChildTreeNode<>("I");
        TreeNode<String> j = new ChildTreeNode<>("J");
        TreeNode<String> k = new ChildTreeNode<>("K");
        TreeNode<String> l = new ChildTreeNode<>("L");

        d.addLeft(h);
        d.addRight(i);

        e.addLeft(j);

        b.addLeft(d);
        b.addRight(e);

        c.addLeft(f);
        c.addRight(g);

        a.addLeft(b);
        a.addRight(c);

        f.addRight(k);
        g.addLeft(l);

        return a;
    }

    private TreeNode<String> createMockNodeNullLeftNode() {
        TreeNode<String> a = new RootTreeNode<>("A");
        TreeNode<String> b = new ChildTreeNode<>("B");
        TreeNode<String> c = new ChildTreeNode<>("C");
        TreeNode<String> d = null;

        b.addLeft(d);

        a.addLeft(b);
        a.addRight(c);

        return a;
    }

    private TreeNode<String> createMockNodeNullRightNode() {
        TreeNode<String> a = new RootTreeNode<>("A");
        TreeNode<String> b = new ChildTreeNode<>("B");
        TreeNode<String> c = new ChildTreeNode<>("C");
        TreeNode<String> d = null;

        b.addRight(d);

        a.addLeft(b);
        a.addRight(c);

        return a;
    }

    private TreeNode<String> createMockNodeNullValues() {
        TreeNode<String> a = new RootTreeNode<>(null);
        TreeNode<String> b = new ChildTreeNode<>(null);
        TreeNode<String> c = new ChildTreeNode<>(null);

        a.addLeft(b);
        a.addRight(c);

        return a;
    }
}
